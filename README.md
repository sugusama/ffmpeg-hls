# FFmpeg HLS

This is a demonstration on how to create an Apple HTTP Live Stream (HLS) using
FFmpeg.

HLS is supported on:

* iOS
* Mac (Safari browser)
* Windows 10 (Edge browser)
* Android 3 or later
* Flash (with a HLS plugin)

To create a HLS video stream you need to install FFmpeg. If you are running
Linux, a package will probably be available from the distribution repositories.
Otherwise you have to compile from source. You should use a recent version of
FFmpeg (version 1.2 or later).

Ubuntu / Debian:
   
```
sudo apt-get install ffmpeg
```

Fedora / CentOS:
```
yum install ffmpeg
```

openSUSE (first install the Packman community repository):
```
sudo zypper install ffmpeg
```

For Windows download binaries from:  
http://ffmpeg.zeranoe.com/builds/

For Mac download binaries from:  
http://www.evermeet.cx/ffmpeg/

Get the sources from the Sintel Open Movie Trailer:  
http://media.xiph.org/sintel/  
sintel_trailer-720-png.tar.gz (images)  
sintel_trailer-audio.flac (sound)  

Unpack the images:
```
tar -xvf sintel_trailer-720-png.tar.gz
```

To create the HLS stream run:
```
./build.sh
```

On Windows you can install [Cygwin](http://cygwin.com/) or
[cmder](http://gooseberrycreative.com/cmder/) to run shell scripts.

If all runs smoothly, you can view the movie in your browser (if it supports
HLS) by opening:
```
sintel-trailer.html
```

To serve HLS from Apache add the correct MIME type to your configuration:
```
AddType application/x-mpegurl .m3u8
AddType video/mp2t .ts
```

Using MIME type application/vnd.apple.mpegURL will not work on most Android
devices. HLS support is available on Android 3.0 or later, but seems to be
quite picky about the HLS formatting and web server configuration.

## References
* http://ffmpeg.org/
* https://www.ffmpeg.org/ffmpeg-formats.html#hls-1
* http://www.longtailvideo.com/support/jw-player/28856/using-apple-hls-streaming
* http://dice.neko-san.net/2013/02/encoding-hls-video-for-jw-player-6/
* https://www.adobe.com/devnet/devices/articles/mobile_video_encoding.html
* https://sonnati.wordpress.com/2012/07/02/ffmpeg-the-swiss-army-knife-of-internet-streaming-part-v/
* https://developer.apple.com/library/ios/#documentation/NetworkingInternet/Conceptual/StreamingMediaGuide/UsingHTTPLiveStreaming/UsingHTTPLiveStreaming.html#//apple_ref/doc/uid/TP40008332-CH102-SW1
* https://developer.apple.com/library/ios/qa/qa1767/_index.html
* http://ffmpeg.org/pipermail/ffmpeg-user/2013-May/015102.html
* http://www.sintel.org/
* http://www.longtailvideo.com/blog/31646/the-pain-of-live-streaming-on-android/
* http://www.streamingmedia.com/Articles/Editorial/Featured-Articles/Apple-Changes-Encoding-Recommendations-for-HLS-96173.aspx
* http://en.opensuse.org/Additional_package_repositories#Packman
* http://flashls.org/

## Author
[Walter Ebert](http://walterebert.com/)